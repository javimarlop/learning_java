# Object-oriented programming in Java

* **Class**: a blueprint for how a data structure should function. A class describes what the object will be, but is separate from the object itself

* **Packages** are used to avoid name conflicts and to control access to classes. A package can be defined as a group made up of similar types of classes, along with sub-packages

* **Constructor**: instructs the class to set up the initial state of an object

* **Object**: instance of a class that stores the state of a class

* **Method**: set of instructions that can be called on an object

* **Parameter**: values that can be specified when creating an object or calling a method

* **Return value**: specifies the data type that a method will return after it runs

* **Value types** are the basic types, and include byte, short, int, long, float, double, boolean, and char

* Use the **final** keyword to mark a variable constant, so that it can be assigned only once

* When you declare a variable or a method as **static**, it belongs to the class, rather than to a specific instance. It's a common practice to use upper case when naming a static variable, although not mandatory

* There are a number of other methods available in the **Math class**, including:
sqrt() for square root, sin() for sine, cos() for cosine, and others

* **Inheritance**: allows one class to use functionality defined in another class

* **For Loops**: used to repeatedly run a block of code

* **For Each Loops**: a concise version of a for loop

* **ArrayList**: stores a list of data

* **HashMap**: stores keys and associated values like a dictionary

* **abstract class** 

* **interface** is a completely abstract class that contains only abstract methods

* Use the **implements** keyword to use an interface with your class. A class can inherit from just one superclass, but can implement multiple interfaces!

* An **abstract method** is a method that is declared without an implementation (without braces, and followed by a semicolon): `abstract void walk();`


