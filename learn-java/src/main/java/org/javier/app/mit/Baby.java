package org.javier.app.mit;

public class Baby {

String name;
double weight = 5.0;
boolean isMale;
int numPoops = 0;
Baby[] siblings;
static int numBabiesMade = 0;

Baby(String myname, boolean maleBaby){
numBabiesMade += 1;
name = myname;
isMale = maleBaby;
}

void sayHi() {
System.out.println(
"Hi, my name is " + name);
}

void eat(double foodWeight) {
if (foodWeight >= 0 &&
foodWeight < weight) {
System.out.println("My weight is "+ weight);
weight = weight + foodWeight;
System.out.println("My weight after eating is "+ weight);
}
}

static void cry(Baby thebaby) { 
System.out.println(thebaby.name + " cries");
}

void cry() {
System.out.println(name + " cries");
}

void whoami() {
System.out.println("I am "+name);
}

public static void main (String[] arguments) {

Baby shiloh = new Baby("Shiloh Jolie-Pitt", true);
Baby knox = new Baby("Knox Jolie-Pitt", true);
Baby pepe = new Baby("Pepe Perez", true);

//Baby.numBabiesMade = 100;

System.out.println(shiloh.name);
System.out.println(shiloh.numPoops);
shiloh.sayHi();
shiloh.eat(1);

System.out.println("shiloh nr. babies made is "+shiloh.numBabiesMade);
System.out.println("knox nr. babies made is "+knox.numBabiesMade);

cry(knox);
shiloh.cry();
pepe.cry();
pepe.whoami();

}

}
