package org.javier.app.examples;

import java.io.*;

class helloworld {
    int myInt;      // this is a class variable that is unique to each object
    static int myInt2;  // this is a class variable shared by all objects of this class

    public static void main (String [] args) {
        // this is the main entry point for this Java application
        System.out.println ("Hello, World\n");
        myInt2 = 14;    // able to access the static int
        helloworld myWorld = new helloworld();
        myWorld.myInt = 32;   // able to access non-static through an object
        System.out.println (myInt2);
        System.out.println (myWorld.myInt);
    }
}
