package org.javier.app.examples;

public class Puppy0{

   public Puppy0(String name){
      // This constructor has one parameter, name.
      System.out.println("Passed Name is: " + name ); 
   }
   
   public static void main(String []args){
      // Following statement would create an object myPuppy
      Puppy0 myPuppy0 = new Puppy0( "tommy" );
   }
}
