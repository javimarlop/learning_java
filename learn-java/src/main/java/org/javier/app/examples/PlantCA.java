package org.javier.app.examples;

public class PlantCA {

int vegetation [][][]= new int[2][66][66];
double grow[]={0.0 ,1.0 ,0.2 ,0.0 ,0.6 ,0.6 ,0.1 ,0.4};
double seed[]={0.0 ,0.2 ,0.2 ,1.0 ,0.2 ,0.6 ,0.6 ,0.5};
double tissue[]={0.0 ,0.0 ,0.95,0.0 ,0.8 ,0.0 ,0.8 ,0.7};
int now =0;
int next=1;
double disturbance;
double resource;

public void VegDeath(int x, int y){
 vegetation[now][x][y] =0;
 vegetation[next][x][y] =0;
}

public void Maintain(int x, int y){
if((Math.random()<resource)||(Math.random()<tissue[vegetation[now][x][y]])){
vegetation[next][x][y] = vegetation[now][x][y];
}else VegDeath(x,y);
}

public void Grow(int x, int y){
double gw [] = new double[9];
double sw [] = new double[9];
int well [] = new int[9];
double choice=0.0;
double gtotal=0.0;
double stotal =0.0;
int i=0;
for(int xx=x-1; xx<=x+1; xx++){
for(int yy=y-1; yy<=y+1; yy++){
gtotal+= grow[vegetation[now][xx][yy]];
stotal+= seed[vegetation[now][xx][yy]];
gw[i]=gtotal;
sw[i]=stotal;
well[i]= vegetation[now][xx][yy];
i++;
}
}
choice =Math.random()*9.0d;
if(choice<gtotal){
while(gw[i]<choice) i++;
vegetation[next][x][y]=well[i];
}
if(vegetation[next][x][y]==0){
choice =Math.random()*stotal;
if(Math.random()<(1-Math.exp(-stotal/3.0d))){
i=0;while(sw[i]<choice) i++;
vegetation[next][x][y]=well[i];
}
}
}

public void Iterate(){
for(int x=1;x<65;x++){
for(int y=1;y<65;y++){
if(Math.random()<disturbance) VegDeath(x,y);
Maintain(x,y);
}
}
for(int x=1;x<65;x++){
for(int y=1;y<65;y++){
if(Math.random()<resource) Grow(x,y);

//int len = 66;
//int c, d;
      //for ( c = 0 ; c < len ; c++ )
      //{
         //for ( d = 0 ; d < len ; d++ )
            //System.out.print(vegetation[1][c][d]);//+"\t");
            System.out.print(vegetation[now][x][y]);//+"\t");
         System.out.println();
      //}

}
//System.out.println(x);
}
int n= now;
now=next;
next=n;
}

	public static void main(String[] args) throws Exception{
		PlantCA testca = new PlantCA();
		testca.Iterate();
		//System.out.println(testca.vegetation.getClass());
		//System.out.println(testca.vegetation[1][1][1]);
		//System.out.println("testca: " + testca);
		//System.out.println("vegetation: " + testca.vegetation);

//int len = 66;
//int c, d;
//      for ( c = 0 ; c < len ; c++ )
//      {
//         for ( d = 0 ; d < len ; d++ )
//            System.out.print(testca.vegetation[1][c][d]+"\t");
// 
//         System.out.println();
//      }
	}

}
