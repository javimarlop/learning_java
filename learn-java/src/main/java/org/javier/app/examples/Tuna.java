package org.javier.app.examples;

public class Tuna {
	public void simpleMessage(String name){
		System.out.println("Hello " + name);
	};
	
	private String girlName;
	
	public Tuna(String name){
		girlName = name;
	}
	
	public void setName(String name){
		
		girlName = name;
	}
	
	public String getName(){
		return girlName;
	}
	
	public void saying(){
		System.out.printf("Your first gf was %s", getName());
	}
}
