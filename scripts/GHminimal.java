package org.javier.app.examples;

/**
 * GHminimal.java
 * @author Joerg R. Weimar
 */
import de.tubs.cs.sc.casim.*;
import java.awt.*;
import java.util.Vector;

/**
 * GHminimal
 */

public class GHminimal extends State { 
    

    protected final static Color[] colors = new Color[3];

    /** 
    * the state of each cell is stored here.
    */
    protected int state;
    
    static {
	colors[0] = null;
	colors[1] = new Color(200,200,0);
	colors[2] = new Color(255,0,0);
    }

    public GHminimal() { 
	state = 0;
    }
    
    /** 
    * Initialization can access all cells and depend on an integer option.
    */
    public static void  initialize(Lattice l, int option) { 
    	switch(option){
    	    case 1: // off-centered
    	    	((GHminimal)l.getState(3*l.getX()/4,l.getY()/2,l.getZ()/2)).state = 2;
    	    	break;
    	    case 2: // spiral
    	    	for (int k=0; k<l.getZ(); k++){
    	    	for (int i=l.getX()/2; i<l.getX(); i++){
    	    	   ((GHminimal)l.getState(i,l.getY()/2,k)).state = 2;
    	    	   ((GHminimal)l.getState(i,l.getY()/2-1,k)).state = 1;
    	    	}}
    	    	break;
    	    default:
	    	((GHminimal)l.getState(l.getX()/2,l.getY()/2,l.getZ()/2)).state = 2;
	}
    }
    
    public void copy(State s) { 
        state = ((GHminimal)s).state;
    }

    public Color getColor() {
      return colors[state];
    }

    public void transition(Cell cell) { 
        int nc = 0;
        
        if (state>0) { 
            state--;
        } else { 
            State neighbors[] = cell.getNeighbors();
            if (neighbors!=null) { 
                int i,c;
                c = neighbors.length;
                for (i=0; i<neighbors.length; i++) { 
                    if (((GHminimal)neighbors[i]).state == 2) { 
                        state = 2;
                        return;
                    }
                }
            }
        }
    }
    
    static GHminimal constantValue = new GHminimal();
    
    static {
    	constantValue.state =1;
    }
    
    public State getConstant() { 
        return constantValue;
    }

    public String toString() {
	return ""+state;
    }
}

